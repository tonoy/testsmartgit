New lines are adding 
New lines are adding 
New lines are adding 
New lines are adding 
New lines are adding 
New lines are adding 


This pages is changed
<section class="content">
    <div class="row">
        <div class="col-md-12">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {!! Session::get('success') !!}
                </div>
                @endif
            @if(Session::has('error'))
            <div class="alert alert-warning">
                {{ Session::get('error') }}				
            </div>
            @endif                                
            <div class="box box-primary">
                {!! Form::open(['url' => '/apps/tracking-no-search', 'method' => 'post', 'class' => 'form', 'id'=>'','role' => 'form']) !!}
                <div class="box-body">
                        <div class="col-md-4">
                            <p>Tracking Number</p>
                            {!! $errors->first('tracking_no','<span class="text-danger">:message</span>') !!}
                            <div class="input-group input-group-sm  {{$errors->has('tracking_no') ? 'has-error' : ''}}">
                                {!! Form::text('tracking_no','',['class'=>'required form-control','placeholder'=>'Tracking Number']) !!}                                                                                              
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="tracking_no_search">Search</button>
                              </span>
                                
                            </div><!-- /input-group -->
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>                                          
                </div>
                {!! Form::close() !!}
            </div>
        </div>     <!-- tracking searching part end here-->  
        <!-- Tracking table starting-->
        @if(!empty($result[0]))
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                        <div class="col-sm-12">
                           <div class="col-sm-4 invoice-col">
                               <strong>Tracking No.</strong><br>
                           </div>
                           <div class="col-sm-4 invoice-col">
                               <strong>Company Name</strong><br>
                           </div>
                           <div class="col-sm-4 invoice-col">
                               <strong>Company Address</strong><br>
                           </div>  
                       </div>                         
                        <div class="col-sm-12">
                            <div class="col-sm-4 invoice-col">
                               {!! $result[0]->tracking_number !!}
                            </div>
                            <div class="col-sm-4 invoice-col">
                                {!! $result[0]->company_name !!}
                            </div>
                            <div class="col-sm-4 invoice-col">
                                House no: {!! $result[0]->company_house_no !!}<br />
                                Flat no:  {!! $result[0]->company_flat_no !!}<br />
                                Street: {!! $result[0]->company_street !!}<br />
                                City: {!! $result[0]->company_city !!}<br />
                                Zip: {!! $result[0]->company_zip !!}<br />
                                Fax: {!! $result[0]->company_fax !!}<br />
                                Email: {!! $result[0]->company_email !!}<br />
                                Contact Phone: {!! $result[0]->contact_phone !!}                                 
                            </div>  
                        </div>   
                                            
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->  
        
        <div class="col-md-12">             
            <div class="box box-primary">
                {!! Form::open(['url' => '/apps/money-save', 'method' => 'post', 'class' => 'form', 'id'=>'','role' => 'form']) !!}
                <div class="box-body">
                        <div class="col-md-4">
                            <p>New Payment</p>
                            {!! $errors->first('new_payment','<span class="text-danger">:message</span>') !!}
                            <div class="input-group input-group-sm  {{$errors->has('new_payment') ? 'has-error' : ''}}">
                                {!! Form::text('new_payment','',['class'=>'required form-control','placeholder'=>'New Payment']) !!}
                                {!! Form::hidden('app_id','1',['class'=>'required form-control','placeholder'=>'']) !!}
                                {!! Form::hidden('due_payment','0',['class'=>'required form-control','placeholder'=>'']) !!}
                                 {!! Form::hidden('tracking_no',$tracking_no,['class'=>'required form-control','placeholder'=>'']) !!}                                
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat">Submit</button>
                              </span>
                                
                            </div><!-- /input-group -->
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>                                          
                </div>
                {!! Form::close() !!}
            </div>
        </div>     <!-- tracking searching part end here-->         
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Payment Table</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Due Payment</th>
                                <th>New Payment</th>
                                <th>Paid By</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  $sum =0; ?>
                        @foreach ($result as $result_view)
                                <tr>
                                    <td>{!! $result_view->due_payment !!}</td>
                                    <td>
                                                {!! $result_view->new_payment !!}
                                                 <?php  $sum = $sum +   $result_view->new_payment; ?>
                                    </td>
                                    <td>{!! $result_view->member_first_name . ' ' . $result_view->member_middle_name !!}</td>
                                   <!-- <td>{!! CommonFunction::updatedOn($result_view->created_at) !!}</td>-->
                                    <td>{!! $result_view->created_at !!}</td>
                                </tr>
                                @endforeach		
                        </tobdy>
                        <tfoot>
                          <tr>
                            <td></td>
                            <td>Total: <strong>{!! $sum !!}</strong> BDT</td>
                            <td></td>
                            <td></td>
                          </tr>
  </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        @endif
        <div class="test"></div>
    </div>   <!-- /.row -->
</section>
<script src="{{ asset('front-end/js/jquery-1.11.1.min.js') }}"></script>
<style>
    .error{border-color: red !important}
</style>
<script>
        /*
         $(document).ready(function(){
         $("#tracking_no_search").click(function(){
         var track_number = $(".tracking_no").val();
         var _token = $('input[name="_token"]').val();
         $.ajax({
         url: './trackingNoSearch',
         type: 'POST',
         data: {sql: sql, _token: _token},
         dataType: 'text',
         success: function (data) {
         console.log(data);
         //alert(data);
         $('.test').html(data);
         },
         error:function (jqXHR, textStatus, errorThrown) {
         alert("error");
         }
         });    
         
         })
         });*/
</script>


@endsection
@section('footer-script')
@endsection

