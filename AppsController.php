<?php

namespace App\Modules\Apps\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\Encryption;
use App\Libraries\CommonFunction;
use App\Http\Requests\AppsRequest;
use App\Modules\Apps\Models\Apps;
use App\Modules\Apps\Models\AppsJob;
use App\Modules\Apps\Models\AppsTestSample;
use App\Modules\Apps\Models\AppsPayment;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\CompanyAssoc;
use App\Modules\Settings\Models\Parameter;
use App\Modules\Settings\Models\Method;
use App\Modules\Settings\Models\TestName;
use Illuminate\Http\Request;
use DB;
use Session;

class AppsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $getList = Apps::leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
//            leftJoin('companies as c', 'c.company_id', '=', 'apps_details.company_id')
                ->where('apps_details.user_id', '=', 1)
                ->orderBy('apps_details.created_at', 'desc')
                ->get(['apps_details.app_id', 'apps_details.tracking_number', 'c.company_name', 'apps_details.app_status', 'apps_details.created_at']);
        return view("apps::list", compact('getList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $testSample = ['' => 'Select one'] + TestName::lists('tst_name', 'tst_id')->all();
        $company = CompanyAssoc::join('companies as c', 'c.company_id', '=', 'company_assoc.company_id')
                ->where('company_assoc.member_id', '=', DB::raw(1))
                ->orderBy('c.company_name')
                ->get(['c.company_name', 'c.company_id']);

        $companyList = ['' => 'Select one'];
        foreach ($company as $row) {
            $companyList[$row->company_id] = $row->company_name;
        }
        return view("apps::create", compact('testSample', 'companyList'));
    }

    /**
     * @param AppsRequest $request
     * @return \Illuminate\View\View
     */
    public function store(AppsRequest $request) {
        $insert = Apps::create([
                    'tracking_number' => rand(1000, 11000000),
                    'app_status' => 0,
                    'company_id' => $request->get('company_id'),
                    'user_id' => 1
        ]);

        $jobInsert = AppsJob::create([
                    'app_id' => $insert->id,
                    'job_name' => $request->get('job_name'),
                    'tst_id' => $request->get('tst_id')[0]
        ]);

        if ($request->get('method_id')) {
            foreach ($request->get('method_id') as $row) {
                AppsTestSample::create([
                    'method_id' => $row,
                    'job_id' => $jobInsert->id
                ]);
            }
        }
//        dd($request->all());
//        return redirect('/apps/' . Encryption::encodeId($insert->id));
        return redirect('/apps/');
    }

    public function bankPayment() {
        return view('apps::bank_payment');
    }

//    public function trackingNoSearch($money_save_tracking="") {
//    \DB::enableQueryLog();
//    if(isset($_POST['tracking_no']))
//    {
//        $track_no = $_POST['tracking_no'];
//    }
// else 
//     {
//        $track_no = $money_save_tracking;
//    }
////echo $track_no;
////die();
//    $result = Apps::leftJoin('companies','companies.company_id','=','apps_details.company_id')
//        ->leftJoin('apps_payment','apps_payment.app_id','=','apps_details.app_id')
//        ->leftJoin('members','members.member_id','=','apps_details.user_id')
//        ->where('apps_details.tracking_number',"=", $track_no)
//         ->select('apps_details.tracking_number', 'members.member_first_name', 'members.member_middle_name', 'members.member_last_name', 'apps_payment.created_at AS paid_time', 'apps_payment.due_payment', 'apps_payment.new_payment', 'companies.company_name','companies.company_house_no', 'companies.company_flat_no','companies.company_street','companies.company_city','companies.company_zip','companies.company_fax','companies.company_email','companies.contact_phone')
//           ->get();
////    print_r($result);
////    die();
//    return view("apps::bank_payment", compact('result','track_no'));
//    }
    public function trackingNoSearch(Request $request) {
        $this->validate($request, ['tracking_no' => 'required|numeric']);
        $encrypted_tracking_no = Encryption::encodeId($request->get('tracking_no'));
        return redirect('apps/bank-payment-process/' . $encrypted_tracking_no);
    }

    public function bankPaymentProcess($tracking_no) {
        $tracking_no = Encryption::decodeId($tracking_no);
        //echo $tracking_no;
        //exit();
        $result = Apps::leftJoin('companies', 'companies.company_id', '=', 'apps_details.company_id')
                ->leftJoin('apps_payment', 'apps_payment.app_id', '=', 'apps_details.app_id')
                ->leftJoin('members', 'members.member_id', '=', 'apps_details.user_id')
                ->where('apps_details.tracking_number', "=", $tracking_no)
                ->select('apps_details.tracking_number', 'members.member_first_name', 'members.member_middle_name', 'apps_payment.created_at', 'apps_payment.due_payment', 'apps_payment.new_payment', 'companies.company_name', 'companies.company_house_no', 'companies.company_flat_no', 'companies.company_street', 'companies.company_city', 'companies.company_zip', 'companies.company_fax', 'companies.company_email', 'companies.contact_phone')
                ->orderBy('apps_payment.created_at', 'desc')
                ->get();

       if(empty($result[0]))
        {
                Session::flash('error', 'No data found!');  
        }         
        return view("apps::bank_payment", compact('result', 'tracking_no'));
    }

    public function moneySave(Request $request) {
         $this->validate($request, ['new_payment' => 'required|numeric']);
        $encrypted_tracking_no = Encryption::encodeId($request->get('tracking_no'));
        $insert = AppsPayment::create([
                    'app_id' => $request->get('app_id'),
                    'due_payment' => $request->get('due_payment'),
                    'new_payment' => $request->get('new_payment'),
                    'user_id' => 1, //session user id
        ]);
        Session::flash('success', 'Successfully Paid <code>' . $request->get('new_payment') . '</code>Taka Only.');
        //return view("apps::bank_payment");
        return redirect('apps/bank-payment-process/' . $encrypted_tracking_no);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * @param $param request type for ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxRequest($param, Request $request) {
        $data = ['responseCode' => 0];
        if ($param == 'parameter') {
            $list = Parameter::where('tst_id', $request->get('id'))->orderBy('param_name')->lists('param_name', 'param_id');
            $data = ['responseCode' => 1, 'data' => $list];
        } elseif ($param == 'company') {
            $list = Company::where('company_id', $request->get('id'))->orderBy('company_name')->get([DB::raw('concat("House: ",company_house_no,", Flat: ",company_flat_no,", Street: ",company_street,", ",company_city,"-",company_zip) location'), DB::raw('concat(contact_person,", Phone: ",contact_phone,", Email: ",contact_email) contact')]);
            $data = ['responseCode' => 1, 'data' => $list[0]];
        } elseif ($param == 'method') {
            $list = Method::leftJoin('tbl_parameter as tp', 'tbl_method.param_id', '=', 'tp.param_id')
                    ->whereIn('method_id', $request->get('id'))
                    ->orderBy('tp.param_name')
                    ->orderBy('method_name')
                    ->get([DB::raw('concat(method_name, " /", tp.param_name) method_name'), 'method_id']);
//            $responseData = '';
//            foreach($list as $row){
//                $responseData .= '<label class="col-md-12"><input type="checkbox" name="method_id[]" value="'. $row->method_id.'"> <b>'. $row->method_name . '</b> (' . $row->param_name.')</label>';
//            }

            $data = ['responseCode' => 1, 'data' => $list];
        }
        return response()->json($data);
    }

}
